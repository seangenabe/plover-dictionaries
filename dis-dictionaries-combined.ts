#!/usr/bin/env -S deno run --allow-read --allow-write

import { join } from "https://deno.land/std@0.133.0/path/mod.ts"

const statuses = await Promise.all([
  Deno.permissions.request({ name: "read" }),
  Deno.permissions.request({ name: "write" }),
])

if (statuses.some((status) => status.state === "denied")) {
  throw new Error("Permissions denied")
}

const denylist = new Set([
  "O/RE", // overcomplicated
  "AD/STAEUT", // Conflicts with "add state"
  "WOFPB", // One-off reference to CSSConf
  "AEU/KOS", // Conflicts with AEU -> "a", rare prefix {eicosa^}
  "AS/KAOE", // prefer "as key" to "ASCII"
  "RERPBG", // remembering (prefer RERG), conflicts with "rendering"
  "SROFLD", // involved, redundant -d
  "A/PHEPB/PHE", // -> anemone, mispronounced
  "A/PHEPB/PHEU", // -> anemone, mispronounced
])

const filterDictionaries = new Set([
  "typey-type",
  "abbreviations",
  "code",
  "condensed-strokes",
  "condensed-strokes-fingerspelled",
  "currency",
  "dict-en-AU-with-extra-stroke",
  "dict",
  "misstrokes",
  "nouns",
  "top-1000-words",
  "top-10000-project-gutenberg-words",
])

const enabledDictionaries = [
  "abbreviations",
  "apps",
  "briefs",
  "code",
  // "computer-powerups",
  "condensed-strokes",
  "currency",
  // "emoji",
  "fingerspelling",
  "git",
  "html",
  "javascript",
  "lorem",
  "markdown",
  "nouns",
  // "numbers",
  "observable",
  "plover-use",
  "plover-powerups",
  "proper-nouns",
  "react",
  "shortcuts",
  "dict",
]

const lowercaseLetters = [
  "A*",
  "PW*",
  "KR*",
  "TK*",
  "*E",
  "TP*",
  "TKPW*",
  "H*",
  "*EU",
  "SKWR*",
  "K*",
  "HR*",
  "PH*",
  "TPH*",
  "O*",
  "P*",
  "KW*",
  "R*",
  "S*",
  "T*",
  "*U",
  "SR*",
  "W*",
  "KP*",
  "KWR*",
  "STKPW*",
]

const rightHandedLetters = [
  "A",
  "PW-",
  "KR-",
  "TK-",
  "E",
  "TP-",
  "TKPW-",
  "H-",
  "EU",
  "SKWR-",
  "K-",
  "HR-",
  "PH-",
  "TPH-",
  "O",
  "P",
  "KW-",
  "R-",
  "S-",
  "T-",
  "U",
  "SR-",
  "W",
  "KP-",
  "KWR-",
  "STKPW-",
]

const allLetters = [
  ...lowercaseLetters,
  ...lowercaseLetters.map((c) => `${c}P`),
]

function filterFingerspelledEntries(source: Record<string, string>) {
  const allLettersSet = new Set(allLetters)
  const rightHandedLettersSet = new Set(
    rightHandedLetters.map((c) => `${c}RBGS`)
  )

  for (const stroke of Object.keys(source)) {
    const strokeComponents = stroke.split("/")
    if (
      strokeComponents.every((component) => allLettersSet.has(component)) ||
      strokeComponents.every((component) =>
        rightHandedLettersSet.has(component)
      )
    ) {
      console.log(`Excluding fingerspelling ${stroke} -> ${source[stroke]}`)
      delete source[stroke]
    }
  }

  return source
}

const dictionaries = await Promise.all(
  enabledDictionaries.map(async (dictionaryName) => {
    const dictionaryContents = await Deno.readTextFile(
      join("steno-dictionaries", "dictionaries", `${dictionaryName}.json`)
    )
    const entries = JSON.parse(dictionaryContents) as Record<string, string>

    // Remove fingerspelled entries
    //   (Fingerspelled entries sometimes conflict with spacing when actually
    //    fingerspelling something.)
    if (filterDictionaries.has(dictionaryName)) {
      filterFingerspelledEntries(entries)
    }

    for (const [stroke, translation] of Object.entries(entries)) {
      // Remove redundant /-D entries
      if (stroke.endsWith("/-D") && translation.endsWith("ed")) {
        const trimmedTranslation = entries[stroke.slice(0, -3)]
        if (
          trimmedTranslation === translation.slice(0, -2) || // Match -ed
          trimmedTranslation === translation.slice(0, -1) // Match -d
        ) {
          console.log(`Excluding redundant -ed/-d ${stroke} --> ${translation}`)
          delete entries[stroke]
        }
      }

      // Remove redundant /-Z entries
      if (
        (stroke.endsWith("/-Z") || stroke.endsWith("/-S")) &&
        /e?s$/.test(translation)
      ) {
        const trimmedTranslation = entries[stroke.slice(0, -3)]
        if (
          trimmedTranslation === translation.slice(0, -2) || // Match -es
          trimmedTranslation === translation.slice(0, -1) // Match -s
        ) {
          console.log(`Excluding redundant -es/-s ${stroke} --> ${translation}`)
          delete entries[stroke]
        }
      }

      // Remove redundant -T/
      if (stroke.startsWith("-T/") && translation.startsWith("the ")) {
        const trimmedTranslation = entries[stroke.slice(3)]
        if (trimmedTranslation === translation.slice(4)) {
          console.log(`Excluding redundant "the " ${stroke} --> ${translation}`)
          delete entries[stroke]
        }
      }

      const chordsSet = new Set(stroke.split("/"))

      // Remove entries with KPA
      if (chordsSet.size > 1 && chordsSet.has("KPA")) {
        console.log(`Excluding outline with KPA ${stroke} --> ${translation}`)
        delete entries[stroke]
      }

      // Remove entries with TK-LS
      if (chordsSet.size > 1 && chordsSet.has("TK-LS")) {
        console.log(`Excluding outline with TK-LS ${stroke} --> ${translation}`)
        delete entries[stroke]
      }
    }

    return entries
  })
)

const combined = Object.assign({}, ...dictionaries.reverse())

for (const entry of denylist) {
  if (combined[entry]) {
    console.log(`Excluding denylisted entry ${entry} -> ${combined[entry]}`)
    delete combined[entry]
  }
}

await Deno.writeTextFile(
  "dis-dictionaries-combined.json",
  JSON.stringify(combined, null, 2)
)
