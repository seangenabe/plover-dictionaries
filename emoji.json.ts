#!/usr/bin/env -S deno --allow-read --allow-write

const outputFilename = "emoji.json";
const emojiEnder = "-PBLG";
const inputFilenames = ["./dis-dictionaries-combined.json", "./new-edits.json"];

function invertKv<K, V>(
  map: ReadonlyMap<K, V>,
): ReadonlyMap<V, readonly K[]> {
  const output = new Map<V, K[]>();

  for (const [key, value] of map) {
    if (!output.has(value)) {
      output.set(value, []);
    }
    output.get(value)!.push(key);
  }

  return output;
}

async function buildOutput() {
  const inputContents = await Promise.all(
    inputFilenames.map(async (fn) => {
      const text = await Deno.readTextFile(fn);
      const contents = JSON.parse(text) as Record<string, string>;
      return contents;
    }),
  );
  const inputItems: Record<string, string> = Object.assign(
    {},
    ...inputContents,
  );
  const reverseMap = invertKv(new Map(Object.entries(inputItems)));

  const map: Record<string, string | string[]> = JSON.parse(
    await Deno.readTextFile(
      "./emoji_map.json",
    ),
  ) as Record<string, string>;

  const output: Record<string, string> = {};

  for (const [outputEmoji, keywordsRaw] of Object.entries(map)) {
    const keywords = ([] as string[]).concat(keywordsRaw);
    for (const keyword of keywords) {
      const outlines = reverseMap.get(keyword) ?? [];

      for (const outline of outlines) {
        output[`${outline}/${emojiEnder}`] = outputEmoji;
      }
    }
  }

  output[emojiEnder] = "emoji";

  return output;
}

async function buildFile() {
  const output = await buildOutput();

  await Deno.writeTextFile(outputFilename, JSON.stringify(output));
}

if (import.meta.main) {
  await buildFile();
}
