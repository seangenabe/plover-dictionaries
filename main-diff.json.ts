#!/usr/bin/env -S deno run --allow-read --allow-write --allow-net

import ky from "https://cdn.skypack.dev/ky?dts"

const typeyType = (await ky(
  "https://raw.githubusercontent.com/didoesdigital/typey-type-data/master/dictionaries/typey-type/typey-type.json"
).json()) as Record<string, string>
const main = (await ky(
  "https://raw.githubusercontent.com/openstenoproject/plover/master/plover/assets/main.json"
).json()) as Record<string, string>

const mainEntries = Object.entries(main)
const typeyTypeEntriesReverseMap = new Map(
  Object.entries(typeyType).map(([stroke, translation]) => [
    translation,
    stroke,
  ])
)

const diffedMainEntries = mainEntries.filter(
  ([, translation]) => !typeyTypeEntriesReverseMap.has(translation)
)

const diffedAsObject = Object.fromEntries(diffedMainEntries)

await Deno.writeTextFile(
  "main-diff.json",
  JSON.stringify(diffedAsObject, null, 2)
)
