# Personal public Plover dictionary

* commands.json - Commands
* genshin.json - Genshin Impact
* japanophilia.json - Japanophile vocabulary, anime/manga/game titles, Japanese learning
* overwritables.json - Short strokes usually hogged by misstrokes in the Plover dictionary, or short strokes not already taken by anything, or rearrangements of such entries. Put above `main.json`.

## main-diff.ts

main.json diffed with Typey Type's dictionary.

## surround.ts

A Deno script that generates a JSON dictionary
that uses and abuses the `plover_retro_everything` plugin
to surround text with symbols and control capitalization.
The right-hand side is patterned similarly to Emily's Symbol System.

## Abbreviations theory

* If possible, spell entirely with Plover theory.
* If not,
  * Begin an abbreviation with the first letter.
  * Aggresively spell the next letters with Plover theory.
* Repeat last stroke to capitalize if applicable.
* Repeat finally to expand.
* Only abbreviations three characters and up should be considered.

## Linux commands theory

* For proper nouns that are also spelled lowercase (e.g. possible Linux commands), capitalize the first letter for the proper name and use lowercase for the commands.
* Repeat to capitalize
