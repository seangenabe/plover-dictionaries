from string import ascii_uppercase

LONGEST_KEY = 3

stroke_lookup = {
  "A*P": "A",
  "B*P": "B",
  "KR*P": "C",
  "TK*P": "D",
  "*EP": "E",
  "TP*P": "F",
  "TKPW*P": "G",
  "H*P": "H",
  "*EUP": "I",
  "SKWR*P": "J",
  "K*P": "K",
  "HR*P": "L",
  "PH*P": "M",
  "TPH*P": "N",
  "O*P": "O",
  "P*P": "P",
  "KW*P": "Q",
  "R*P": "R",
  "S*P": "S",
  "T*P": "T",
  "*UP": "U",
  "SR*P": "V",
  "W*P": "W",
  "KP*P": "X",
  "KWR*P": "Y",
  "STKPW*P": "Z",
}

# If this does not render ok in your IDE,
# this is perfectly fine, these are just the regional indicators from A-Z.
regional_letters = "🇦🇧🇨🇩🇪🇫🇬🇭🇮🇯🇰🇱🇲🇳🇴🇵🇶🇷🇸🇹🇺🇻🇼🇽🇾🇿"

regional_map = dict(
  zip(ascii_uppercase, regional_letters),
)

def getUnicodeLetter(stroke):
  if not stroke in stroke_lookup:
    raise KeyError
  print(regional_letters)
  print(stroke_lookup)
  print(stroke)
  c = regional_map[stroke_lookup[stroke]]
  assert c is not None
  return c

def lookup(outline):
  if len(outline) < 3:
    raise KeyError
  if outline[2] != "-PBLG":
    raise KeyError
  
  char1 = getUnicodeLetter(outline[0])
  print(char1)
  char2 = getUnicodeLetter(outline[1])
  print(char2)

  return "{}{}".format(char1, char2)

