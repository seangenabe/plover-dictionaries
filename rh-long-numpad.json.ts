#!/usr/bin/env -S deno run --allow-read --allow-write

import { normalizeStenoStrokes } from "./src/normalize-steno-strokes.ts";
import { getOrderedCombinations } from "./src/ordered-combinations.ts";

const output: Record<string, string> = {};

const digits = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0] as const;

const starter = "#";

const digitsMap = new Map<typeof digits[number], string>([
  [1, "-F"],
  [2, "-P"],
  [3, "-L"],
  [4, "-T"],
  [5, "-D"],
  [6, "-R"],
  [7, "-B"],
  [8, "-G"],
  [9, "-S"],
  [0, "-Z"],
]);

function addToOutput({
  stroke,
  strokes,
  translation,
  translations,
}: {
  stroke?: string;
  strokes?: readonly string[];
  translation?: string;
  translations?: readonly (string | typeof digits[number])[];
}) {
  const allStrokes = [stroke ?? ""].concat(strokes ?? []);
  const allTranslationsStr = (translation ?? "")
    + (translations ?? []).join("");

  output[
    normalizeStenoStrokes([starter, ...allStrokes])[0]
  ] = `{&${allTranslationsStr}}`;
}

for (
  let combinationLength = 1;
  combinationLength <= digits.length;
  combinationLength++
) {
  for (const combination of getOrderedCombinations(combinationLength, digits)) {
    const mainStroke = combination.map((digit) => digitsMap.get(digit)!);
    // Normal combination: [1, 2, 3] => 123
    addToOutput({
      strokes: mainStroke,
      translations: combination,
    });

    // Repeat first digit: [1, 2, 3] => 1123
    addToOutput({
      strokes: ["R", ...mainStroke],
      translations: [combination[0], ...combination],
    });

    // Invert first two digits: [1, 2, 3] => 213
    if (combination.length >= 2) {
      addToOutput({
        strokes: ["H", ...mainStroke],
        translations: [combination[1], combination[0], ...combination.slice(2)],
      });
    }

    // Times ten: [1, 2, 3] => 1230
    addToOutput({
      strokes: ["A", ...mainStroke],
      translations: [...combination, "0"],
    });

    // Times 100: [1, 2, 3] => 12300
    addToOutput({
      strokes: ["O", ...mainStroke],
      translations: [...combination, "00"],
    });

    // Times 1000: [1, 2, 3] => 123000
    addToOutput({
      strokes: ["AO", ...mainStroke],
      translations: [...combination, "000"],
    });
  }
}

await Deno.writeTextFile(
  "rh-long-numpad.json",
  JSON.stringify(output, null, 2),
);
