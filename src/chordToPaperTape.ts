const stenoLetters = "#STKPWHRAO*EUFRPBLGTSDZ"

export function chordToPaperTape(chord: number) {
  return chord
    .toString(2)
    .padStart(23, "0")
    .split("")
    .map((c, i) => (c === "1" ? stenoLetters[i] : " "))
    .join("")
}
