export function* combinations<T>(
  arr: readonly T[][],
  out: readonly T[] = []
): Iterable<readonly T[]> {
  if (arr.length === 0) {
    yield out
    return
  }

  const [firstArr, ...rest] = arr
  for (const firstArrItem of firstArr) {
    yield* combinations(rest, [...out, firstArrItem])
  }
}

if (import.meta.main) {
  const { assertEquals } = await import(
    "https://deno.land/std/testing/asserts.ts"
  )

  const actual = [
    ...combinations([
      ["a", "b"],
      ["c", "d"],
    ]),
  ]

  const expected = [
    ["a", "c"],
    ["a", "d"],
    ["b", "c"],
    ["b", "d"],
  ]
  assertEquals(actual, expected)
}
