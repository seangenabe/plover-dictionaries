import { gray, yellow } from "https://deno.land/std@0.111.0/fmt/colors.ts"

export function formatContent(content: string) {
  return content.replaceAll(/\#[^~(}]+(\([^)]+?\))?/g, (match) => yellow(match))
}

export function formatTranslation(translation: string) {
  return translation.replaceAll(
    /({\^?(?:\~\|)?)(\-\||\&?|\*?\-\||\*?(?:\<\>))(.*?)(\^?})/g,
    (_, open, amp, content, close) =>
      `${gray(open)}${yellow(amp ?? "")}${formatContent(content)}${gray(close)}`
  )
}
