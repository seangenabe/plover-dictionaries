#!/usr/bin/env -S deno run --allow-read --allow-net

import ky from "https://cdn.skypack.dev/ky?dts"
import { blue } from "https://deno.land/std@0.111.0/fmt/colors.ts"
import { formatTranslation } from "./format-translation.ts"

const typeyType = (await ky(
  "https://raw.githubusercontent.com/didoesdigital/typey-type-data/master/dictionaries/typey-type/typey-type.json"
).json()) as Record<string, string>

for (const [stroke, translation] of Object.entries(typeyType)) {
  if (translation.startsWith("{^") || translation.endsWith("^}")) {
    console.log(`${blue(stroke)}: ${formatTranslation(translation)}`)
  }
}
