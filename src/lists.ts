export const lowercaseLetters: ReadonlyMap<string, string> = new Map([
  ["A*", "a"],
  ["PW*", "b"],
  ["KR*", "c"],
  ["TK*", "d"],
  ["*E", "e"],
  ["TP*", "f"],
  ["TKPW*", "g"],
  ["H*", "h"],
  ["*EU", "i"],
  ["SKWR*", "j"],
  ["K*", "k"],
  ["HR*", "l"],
  ["PH*", "m"],
  ["TPH*", "n"],
  ["O*", "o"],
  ["P*", "p"],
  ["KW*", "q"],
  ["R*", "r"],
  ["S*", "s"],
  ["T*", "t"],
  ["*U", "u"],
  ["SR*", "v"],
  ["W*", "w"],
  ["KP*", "x"],
  ["KWR*", "y"],
  ["STKPW*", "z"],
])

export const uppercaseLetters: ReadonlyMap<string, string> = new Map(
  [...lowercaseLetters.entries()].map(([stroke, translation]) => [
    `${stroke}P`,
    translation.toUpperCase(),
  ])
)

export const numerals = [
  "0",
  "1",
  "2",
  "3",
  "4",
  "5",
  "6",
  "7",
  "8",
  "9",
] as ReadonlyArray<string>
