import { expect, test } from "bun:test";
import {
  combineChords,
  normalizeStenoStrokes,
  parseStenoChord,
} from "./normalize-steno-strokes.ts";

test("should parse numbers correctly", () => {
  expect(normalizeStenoStrokes(["#S"])).toEqual(["1"]);
  expect(normalizeStenoStrokes(["#T"])).toEqual(["2"]);
  expect(normalizeStenoStrokes(["#P"])).toEqual(["3"]);
  expect(normalizeStenoStrokes(["#H"])).toEqual(["4"]);
  expect(normalizeStenoStrokes(["#-F"])).toEqual(["-6"]);
  expect(normalizeStenoStrokes(["#-P"])).toEqual(["-7"]);
  expect(normalizeStenoStrokes(["#-L"])).toEqual(["-8"]);
  expect(normalizeStenoStrokes(["#-T"])).toEqual(["-9"]);
  expect(normalizeStenoStrokes(["#A"])).toEqual(["5"]);
  expect(normalizeStenoStrokes(["#O"])).toEqual(["0"]);
});

test("should parse chord correctly", () => {
  expect(parseStenoChord("#-P")).toEqual({ lh: "#", vowels: "", rh: "P" });
  expect(parseStenoChord("-7")).toEqual({ lh: "#", vowels: "", rh: "P" });
  expect(parseStenoChord("4")).toEqual({ lh: "#H", vowels: "", rh: "" });
});

test("should combine chords correctly", () => {
  expect(combineChords(["S", "T"])).toBe("ST");
  expect(combineChords(["#", "S"])).toBe("1");
  expect(combineChords(["S", "#"])).toBe("1");
  expect(combineChords(["-P", "#"])).toBe("-7");
});
