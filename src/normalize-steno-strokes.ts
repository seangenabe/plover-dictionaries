export function parseStenoChord(chord: string) {
  const lh = new Set<string>();
  const vowels = new Set<string>();
  const rh = new Set<string>();

  const r = chord.match(/([^AOEU50*-]*)([AOEU50*-]*)(.*)/);
  if (r) {
    if (r[1]) {
      for (const c of r[1]) {
        lh.add(c);
      }
    }
    if (r[2]) {
      for (const c of r[2]) {
        vowels.add(c);
      }
    }
    if (r[3]) {
      for (const c of r[3]) {
        rh.add(c);
      }
    }
  }

  vowels.delete("-");

  if (lh.has("1")) {
    lh.add("#");
    lh.add("S");
    lh.delete("1");
  }
  if (lh.has("2")) {
    lh.add("#");
    lh.add("T");
    lh.delete("2");
  }
  if (lh.has("3")) {
    lh.add("#");
    lh.add("P");
    lh.delete("3");
  }
  if (lh.has("4")) {
    lh.add("#");
    lh.add("H");
    lh.delete("4");
  }
  if (vowels.has("5")) {
    lh.add("#");
    vowels.add("A");
    vowels.delete("5");
  }
  if (vowels.has("0")) {
    lh.add("#");
    vowels.add("O");
    vowels.delete("0");
  }
  if (rh.has("6")) {
    lh.add("#");
    rh.add("F");
    rh.delete("6");
  }
  if (rh.has("7")) {
    lh.add("#");
    rh.add("P");
    rh.delete("7");
  }
  if (rh.has("8")) {
    lh.add("#");
    rh.add("L");
    rh.delete("8");
  }
  if (rh.has("9")) {
    lh.add("#");
    rh.add("T");
    rh.delete("9");
  }

  const lhStr = [..."#STKPWHR"].filter((c) => lh.has(c)).join("");
  const vowelsStr = [..."AOEU"].filter((c) => vowels.has(c)).join("");
  const rhStr = [..."FRPBLGTSDZ"].filter((c) => rh.has(c)).join("");

  return { lh: lhStr, vowels: vowelsStr, rh: rhStr };
}

export function normalizeStenoStrokes(strokes: string[]) {
  return strokes.map((chord) => {
    const parsedChord = parseStenoChord(chord);

    let { lh, vowels, rh } = parsedChord;
    if (
      lh.startsWith("#")
      && (/[STPH]/.test(lh) || /[AO]/.test(vowels) || /[FPLT]/.test(rh))
    ) {
      lh = lh
        .replace("#", "")
        .replace("S", "1")
        .replace("T", "2")
        .replace("P", "3")
        .replace("H", "4");
      vowels = vowels.replace("A", "5").replace("O", "0");
      rh = rh
        .replace("F", "6")
        .replace("P", "7")
        .replace("L", "8")
        .replace("T", "9");
    }

    if (!vowels) {
      return `${lh}${rh ? "-" : ""}${rh}`;
    }
    return lh + vowels + rh;
  });
}

export function sortLh(input: string) {
  let output = "";
  const chars = new Set([...input]);
  for (const c of "#STKPWHR") {
    if (chars.has(c)) {
      output += c;
    }
  }
  return output;
}

export function sortVowels(input: string) {
  let output = "";
  const chars = new Set([...input]);
  for (const c of "AOEU") {
    if (chars.has(c)) {
      output += c;
    }
  }
  return output;
}

export function sortRh(input: string) {
  let output = "";
  const chars = new Set([...input]);
  for (const c of "FRPBLGTSDZ") {
    if (chars.has(c)) {
      output += c;
    }
  }
  return output;
}

export function combineChords(chordTokens: string[]) {
  let outputLh = "";
  let outputVowels = "";
  let outputRh = "";

  for (const chord of chordTokens) {
    const { lh, vowels, rh } = parseStenoChord(chord);
    outputLh += lh;
    outputVowels += vowels;
    outputRh += rh;
  }

  let combined = `${sortLh(outputLh)}${sortVowels(outputVowels)}-${
    sortRh(outputRh)
  }`;
  if (combined.includes("*")) {
    combined = combined.replace("-", "");
  }
  return normalizeStenoStrokes([combined])[0];
}
