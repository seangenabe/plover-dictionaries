export function* getOrderedCombinations<T>(
  targetCombinationSize: number,
  remainingItems: readonly T[],
  collectedItems: readonly T[] = []
): Iterable<readonly T[]> {
  // [1, 2], [3, 4, 5], 4
  if (collectedItems.length === targetCombinationSize) {
    yield collectedItems
    return
  }
  const max =
    remainingItems.length - (collectedItems.length - targetCombinationSize) - 1
  for (let i = 0; i < max; i++) {
    yield* getOrderedCombinations(
      targetCombinationSize,
      remainingItems.slice(i + 1),
      [...collectedItems, remainingItems[i]]
    )
  }
}
