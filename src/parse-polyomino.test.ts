import { expect, test } from "bun:test";
import { parsePolyomino } from "./parse-polyomino.ts";

const setup = "TPHKWR".split("") as [
  string,
  string,
  string,
  string,
  string,
  string,
];

const setupRs = ["-F", "-P", "-L", "-R", "-B", "-G"] as const;

test("monominoes", () => {
  expect(parsePolyomino(setup, "nw")).toBe("T");
  expect(parsePolyomino(setup, "up")).toBe("P");
  expect(parsePolyomino(setup, "ne")).toBe("H");
  expect(parsePolyomino(setup, "left")).toBe("K");
  expect(parsePolyomino(setup, "down")).toBe("W");
  expect(parsePolyomino(setup, "right")).toBe("R");
});

test("basic", () => {
  expect(parsePolyomino(setup, "domino-nw")).toBe("TP");
});

test("combine with other", () => {
  expect(parsePolyomino(setup, "E + domino-nw")).toBe("TPE");
  expect(parsePolyomino(setup, "domino-nw + E")).toBe("TPE");
});

test("ampersand", () => {
  expect(parsePolyomino(setup, "nw & ne")).toBe("TH");
});

test("combine with #", () => {
  expect(parsePolyomino(setup, "tetromino-o-left + #")).toBe("2K3W");
  expect(parsePolyomino(setup, "# + tetromino-o-left")).toBe("2K3W");
  expect(parsePolyomino(setup, "nw & right + #")).toBe("2R");
});

test("parse rs numbers correctly", () => {
  expect(parsePolyomino(setupRs, "up")).toBe("-P");
  expect(parsePolyomino(setupRs, "up + #")).toBe("-7");
});
