import { combineChords } from "./normalize-steno-strokes.ts";

const polyominoTable = {
  // monominoes // this portion is necessary to complete the table
  nw: "nw",
  up: "up",
  ne: "ne",
  left: "left",
  down: "down",
  right: "right",
  // dominoes
  "domino-left": "nw&left",
  "domino-center": "up&down",
  "domino-right": "ne&right",
  "domino-nw": "nw&up",
  "domino-ne": "up&ne",
  "domino-sw": "left&down",
  "domino-se": "down&right",
  // trominoes
  "tromino-outer-nw": "domino-nw&left",
  "tromino-outer-ne": "domino-ne&right",
  "tromino-outer-sw": "domino-sw&nw",
  "tromino-outer-se": "domino-se&ne",
  "tromino-inner-nw": "domino-ne&down",
  "tromino-inner-ne": "domino-nw&down",
  "tromino-inner-sw": "domino-se&up",
  "tromino-inner-se": "domino-sw&up",
  "tromino-all-up": "nw&up&ne",
  "tromino-all-down": "left&down&right",
  // tetrominoes
  "tetromino-o-left": "domino-nw&domino-sw",
  "tetromino-o-right": "domino-sw&domino-se",
  "tetromino-t": "tromino-inner-se&right",
  "tetromino-t-down": "tromino-inner-ne&ne",
  "tetromino-s": "tromino-inner-se&ne",
  "tetromino-z": "tromino-inner-ne&right",
  "tetromino-j-sw": "domino-left&domino-se",
  "tetromino-j-ne": "domino-nw&domino-right",
  "tetromino-l-se": "domino-sw&domino-right",
  "tetromino-l-nw": "domino-left&domino-ne",
  // pentominoes
  "pentomino-p-sans-right": "tromino-all-up&domino-sw",
  "pentomino-p-sans-left": "tromino-all-up&domino-se",
  "pentomino-p-sans-nw": "domino-ne&tromino-all-down",
  "pentomino-p-sans-ne": "domino-nw&tromino-all-down",
  "pentomino-u-sans-up": "nw&ne&left&down&right",
  "pentomino-u-sans-down": "nw&up&ne&left&right",
  // hexomino
  "hexomino-block": "nw&up&ne&left&down&right",
} as const satisfies Record<string, string>;

export type PolyominoToken = keyof typeof polyominoTable | (string & {});

export function parsePolyominoToken(
  setup: readonly [
    nw: string,
    up: string,
    ne: string,
    left: string,
    down: string,
    right: string,
  ],
  token: PolyominoToken,
): string {
  const [nw, up, ne, left, down, right] = setup;

  if (token.includes("&")) {
    return token.split("&").map((subtoken) =>
      parsePolyominoToken(setup, subtoken.trim())
    )
      .join("");
  }
  // monominoes
  if (token === "nw") {
    return nw;
  }
  if (token === "up") {
    return up;
  }
  if (token === "ne") {
    return ne;
  }
  if (token === "left") {
    return left;
  }
  if (token === "down") {
    return down;
  }
  if (token === "right") {
    return right;
  }

  const tableLookup = (polyominoTable as Record<string, string>)[token];
  if (tableLookup) {
    return parsePolyominoToken(setup, tableLookup);
  }

  return token;
}

// Parse polyominos in a 3x2 space
export function parsePolyomino(
  setup: readonly [
    nw: string,
    up: string,
    ne: string,
    left: string,
    down: string,
    right: string,
  ],
  polyominoStr: PolyominoToken,
) {
  const strokeKeys = polyominoStr.split("+").filter(Boolean).map(
    (token) => parsePolyominoToken(setup, token.trim()),
  );
  const ret = combineChords(strokeKeys);
  return ret;
}
