#!/usr/bin/deno

import { strokeCompare } from "./stroke-compare.ts"

const filename = Deno.args[0]

const obj = JSON.parse(await Deno.readTextFile(filename))

const keys = Object.keys(obj)
const sortedKeys = keys.sort((a, b) => {
  const aArr = a.split("/")
  const bArr = b.split("/")
  return strokeCompare(aArr, bArr)
})

const newObj = Object.fromEntries(sortedKeys.map((key) => [key, obj[key]]))

const str = JSON.stringify(newObj, null, 2).replaceAll("  ", "") + "\n"

await Deno.writeTextFile(filename, str)
