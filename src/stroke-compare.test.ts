import { assertEquals } from "https://deno.land/std@0.181.0/testing/asserts.ts";
import { chordToInt } from "./stroke-compare.ts";

Deno.test("chordToInt", () => {
  assertEquals(chordToInt("-Z"), 1)
  assertEquals(chordToInt("-D"), 2)
  assertEquals(chordToInt("-DZ"), 3)
  assertEquals(chordToInt("-S"), 4)
  assertEquals(chordToInt("-T"), 8)
  assertEquals(chordToInt("-G"), 16)
  assertEquals(chordToInt("-L"), 32)
  assertEquals(chordToInt("-B"), 64)
  assertEquals(chordToInt("-P"), 128)
  assertEquals(chordToInt("-R"), 256)
  assertEquals(chordToInt("-F"), 512)
  assertEquals(chordToInt("U"), 1024)
  assertEquals(chordToInt("E"), 2048)
  assertEquals(chordToInt("*"), 4096)
  assertEquals(chordToInt("O"), 8192)
  assertEquals(chordToInt("A"), 16384)
  assertEquals(chordToInt("R"), 32768)
  assertEquals(chordToInt("H"), 65536)
  assertEquals(chordToInt("W"), 131072)
  assertEquals(chordToInt("P"), 262144)
  assertEquals(chordToInt("K"), 524288)
  assertEquals(chordToInt("T"), 1048576)
  assertEquals(chordToInt("S"), 2097152)
  assertEquals(chordToInt("#"), 4194304)
})
