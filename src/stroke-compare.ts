import QuickLRU from "npm:quick-lru@6.1.2";
import { parseStenoChord } from "./normalize-steno-strokes.ts";

const lru = new QuickLRU<string, number>({ maxSize: 10000 });

const lhMap = {
  "#": 1 << 22,
  S: 1 << 21,
  T: 1 << 20,
  K: 1 << 19,
  P: 1 << 18,
  W: 1 << 17,
  H: 1 << 16,
  R: 1 << 15,
};

const vowelsMap = {
  A: 1 << 14,
  O: 1 << 13,
  "*": 1 << 12,
  E: 1 << 11,
  U: 1 << 10,
};

const rhMap = {
  F: 1 << 9,
  R: 1 << 8,
  P: 1 << 7,
  B: 1 << 6,
  L: 1 << 5,
  G: 1 << 4,
  T: 1 << 3,
  S: 1 << 2,
  D: 1 << 1,
  Z: 1 << 0,
};

export function chordToInt(chord: string, pure = false): number {
  if (!pure && lru.has(chord)) {
    return lru.get(chord)!;
  }
  let result = 0;
  const parsedChord = parseStenoChord(chord);

  for (const c of parsedChord.lh) {
    if (c in lhMap) {
      result |= lhMap[c as keyof typeof lhMap];
    }
  }
  for (const c of parsedChord.vowels) {
    if (c in vowelsMap) {
      result |= vowelsMap[c as keyof typeof vowelsMap];
    }
  }
  for (const c of parsedChord.rh) {
    if (c in rhMap) {
      result |= rhMap[c as keyof typeof rhMap];
    }
  }

  if (!pure) {
    lru.set(chord, result);
  }

  return result;
}

export function chordCompare(a: string, b: string, pure = false) {
  const ret = chordToInt(b, pure) - chordToInt(a, pure);
  return ret;
}

export function strokeCompare(a: string[], b: string[], pure = false) {
  const minLength = Math.min(a.length, b.length);

  for (let i = 0; i < minLength; i++) {
    const comparison = chordCompare(a[i], b[i], pure);
    if (comparison !== 0) {
      return comparison;
    }
  }

  return a.length - b.length;
}
