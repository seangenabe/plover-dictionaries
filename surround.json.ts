#!/usr/bin/env -S deno run --allow-write=surround.json

const PREFIX = "KPH"

// Next: (blank)
// retro_everything: E
// retro: U
// change mode: EU

const map: Record<
  string,
  { next?: string; retro?: string; re?: string; hold?: string }
> = {
  // Uppercase all - THE QUICK BROWN FOX
  "-FPL": {
    next: "{<}",
    retro: "{*<}",
    ...enableMode("{MODE:CAPS}", "{MODE:RESET_CASE}"),
  },
  // Lowercase all - the quick brown fox
  "-RBG": {
    next: "{>}",
    retro: "{*>}",
    ...enableMode("{MODE:LOWER}", "{MODE:RESET_CASE}"),
  },
  // Pascal case - TheQuickBrownFox
  "-FBG": {
    next: "{<}",
    retro: "{*<}",
    ...enableMode("{MODE:TITLE}{MODE:SET_SPACE:}"),
  },
  // Title case - The Quick Brown Fox
  "-FG": {
    next: "{<}",
    retro: "{*<}",
    ...enableMode("{MODE:TITLE}"),
  },
  // Camel case - theQuickBrownFox
  "-RPG": {
    next: "{<}",
    retro: "{*<}",
    ...enableMode("{MODE:CAMEL}"),
  },
  // Snake case - the_quick_brown_fox
  "-BG": {
    next: "{^}_{^}",
    ...enableMode("{MODE:SNAKE}"),
  },
  // Kebab case - the-quick-brown-fox
  "-PL": {
    next: "{^}-{^}",
    ...enableMode("{MODE:SET_SPACE:-}"),
  },
  // JavaScript template string expression - `${the quick brown fox}`
  "-FRPL": symbols("`$\\{", "\\}`"),
  // Period - the.quick.brown.fox
  "-R": symbols("."),
  // Force a space / retroactively add a space
  "-RB": {
    next: "{^ ^}",
    retro: "{*?}",
    re: "=retro_everything:{^ ^},",
    hold: "{^ ^}",
  },
}

const output: Record<string, string> = {
  [PREFIX]: "{MODE:RESET}",
}

for (const [key, value] of Object.entries(map)) {
  setIfHasValue(output, combine(PREFIX, key), value.re)
  setIfHasValue(output, combine(PREFIX, "E", key), value.retro)
  setIfHasValue(output, combine(PREFIX, "U", key), value.next)
  setIfHasValue(output, combine(PREFIX, "EU", key), value.hold)
}

await Deno.permissions.request({ name: "write", path: "surround.json" })

await Deno.writeTextFile("surround.json", JSON.stringify(output, null, 2))

function combine(...strings: string[]) {
  let newString = strings.join("")
  if (/[AOEU]/g.test(newString)) {
    newString = newString.replaceAll("-", "")
  }
  return newString
}

function setIfHasValue<T extends Record<string, unknown>>(
  obj: T,
  key: keyof T,
  value?: T[typeof key] | null
) {
  if (value) {
    obj[key] = value
  }
}

function enableMode(mode: string, reset = "{MODE:RESET}") {
  return {
    re: `=retro_everything:${mode},${reset}`,
    hold: mode,
  }
}

function symbols(start: string, end = start) {
  return {
    next: `{^}${start}{^}`,
    re: `=retro_everything:${start},${end}`,
    hold: `{^}${start}{^}`,
  }
}
