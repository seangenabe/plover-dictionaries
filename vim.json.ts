#!/usr/bin/env bun

import { normalizeStenoStrokes } from "./src/normalize-steno-strokes.ts";

const starter = "TWH";

/**
 * Key to invoke temporary normal mode in Neovim.
 * You should change this to whatever key you don't use.
 * @see :h i_CTRL-o
 * @see https://plover.wiki/index.php/Dictionary_format
 */
const TN = "{#F12}";
/**
 * Enter key press
 */
const LF = "{#Return}";
/**
 * Lowercase
 * @see https://plover.wiki/index.php/Dictionary_format#Uncapitalize_Next_Word
 */
const L = "{>}";
/**
 * Suppress next space
 * @see https://plover.wiki/index.php/Dictionary_format#Suppress_Next_Space
 */
const DLS = "{^}";
const LUA = `:${L}lua `;

const map = {
  // Basic movement

  // Word and token movement
  "-R": `${TN}${DLS}${L}b`,
  "-ER": `${TN}${DLS}B`, // Add -E to move by token
  "-G": `${TN}${DLS}${L}e`,
  "-EG": `${TN}${DLS}E`,
  "-GS": `${TN}${DLS}${L}w`,
  "-EGS": `${TN}${DLS}W`,
  // Jumping -- note that doesn't drop you back to insert mode
  "-RB": `${TN}${DLS}F`,
  "-FRPB": `${TN}${DLS}T`,
  "-BG": `${TN}${DLS}${L}f`,
  "-PBLG": `${TN}${DLS}${L}t`,

  // Paragraph movement
  "-RPG": `${TN}${DLS}\\{${L}i`,
  "-FBL": `${TN}${DLS}\\}${L}i`,

  // Indent line
  "-RPB": `${TN}${DLS}>>${TN}${L}gv`,
  "-PBG": `${TN}${DLS}<<${TN}${L}gv`,
  "-FPBL": `${TN}${DLS}==`,
  "-RPBG": `${TN}${DLS}=`,

  // Whole file movement
  "-FPLG": `${TN}${DLS}${L}gg0`,
  "-RBLG": `${TN}${DLS}G$`,

  // Basic letter editing
  "-F": `{#Left}${TN}${DLS}${L}x`,
  "-L": `${TN}${DLS}${L}x`,

  // Line movements
  "-FPL": `${TN}${DLS}^`,
  "*FP": `${TN}${DLS}0`,
  "-RBG": `${TN}${DLS}$`,

  // Basic word and token editing
  "-FP": `${TN}${DLS}${L}db`,
  "-EFP": `${TN}${DLS}${L}dB`, // Add -E to move by token
  "-PL": `${TN}${DLS}${L}de`,
  "-EPL": `${TN}${DLS}${L}dE`,
  "-PLS": `${TN}${DLS}${L}diw`,
  "-UPLS": `${TN}${DLS}${L}daw`, // Add -U to include whitespace
  "-EPLS": `${TN}${DLS}${L}diW`,
  "-EUPLS": `${TN}${DLS}${L}daW`,
  // Line editing
  "-P": `${TN}${DLS}O`,
  "-PS": `${TN}:call append(line('.')-1, '')${LF}`,
  "-B": `${TN}${DLS}${L}o`,
  "-BS": `${TN}:call append(line('.'), '')${LF}`,
  "-PB": `${TN}${DLS}${L}dd`,
  "-FRB": `${TN}${DLS}${L}d^`,
  "-RLG": `${TN}${DLS}${L}d$`,
  // Open ended (select your character)
  "-FG": `${TN}${DLS}${L}vi`,
  "-EFG": `${TN}${DLS}${L}va`,

  // Marking
  // Add -T to mark stuff
  "-FPT": `${TN}${DLS}${L}vby`,
  "-EFPT": `${TN}${DLS}${L}vBy`,
  "-PLT": `${TN}${DLS}${L}vey`,
  "-EPLT": `${TN}${DLS}${L}vEy`,
  "-FLT": `${TN}${DLS}${L}viwy`,
  "-EFLT": `${TN}${DLS}${L}viWy`,
  "-UPLT": `${TN}${DLS}${L}vaWy`,
  "-EUPLT": `${TN}${DLS}${L}vaWY`,
  "-PBT": `${TN}${DLS}${L}yy`,
  "-RL": `${DLS}${L}%`,

  // Paste
  "-RG": `${TN}${DLS}${L}p`,

  // Tabs
  "-RPBLG": `${TN}${DLS}${L}gt`,
  "-FRPBG": `${TN}${DLS}${L}gT`,

  // Commands
  "-E": `${DLS}${L}i`,
  "-U": `${DLS}${L}a`,
  "#-FG": `${TN}${DLS}:${L}w${LF}`,
  "#-FRPB": `${TN}${DLS}:${L}q${LF}`,
  "#-FRPBLG": `${TN}${DLS}:${L}q!${LF}`,
  "#-FP": `${TN}${DLS}${L}u`,
  "#-PL": `${TN}${DLS}${L}:red${LF}`,
  "#-F": `${TN}{#Control(o)}`,
  "#-L": `${TN}{#Control(i)}`,
  "#-R": `${TN}${DLS}${L}v`,
  "#-RG": `${TN}${DLS}V`,
  "#-G": `${TN}${DLS}:`,
  "#-RBG": `${TN}${DLS}${LUA}${L}vim.lsp.buf.definition()${LF}`,
} as const satisfies Partial<Record<string, string>>;

const cmd = (s: string) => `${TN}${DLS}:${s}${LF}`;

const extras1 = {
  EFBG: cmd(`Oil`), // oil: ex(plore)
  DEF: cmd(`${L}require'telescope.builtin'.lsp_type_definitions{}`), // definition
  REN: cmd(`${L}vim.lsp.buf.rename()`), // ren(ame)
  KRA: cmd(`${L}vim.lsp.buf.code_action()`), // ca
  SEUG: cmd(`${L}vim.lsp.buf.signature_help()`), // sig(nature)
  REFPBT: cmd(
    `${L}require'telescope'.extensions.recent_files.pick{only_cwd=true}`,
  ), // recent
  OEP: cmd(
    `${L}require'telescope'.extensions.recent_files.pick{only_cwd=false}`,
  ), // open
  TP: cmd(`${L}require'telescope.builtin'.find_files{}`), // f(iles)
  TPAOEULS: cmd(`${L}require'telescope.builtin'.find_files{}"`),
  E: `${TN}${DLS}${L}e`, // e
  STKPW: cmd(`${L}require'telescope'.extensions.zoxide.list{}`), // z(oxide)
  KHROR: cmd(`${L}require'telescope.builtin'.colorscheme{}`), // colorscheme
  KPHAPBDS: cmd(`${L}require'telescope.builtin'.commands{}'`), // commands
  HEP: cmd(`${L}require'telescope.builtin'.help_tags{}`), // help
  PHAPB: cmd(`${L}require'telescope.builtin'.man_pages{}`), // man
  PWUFR: cmd(`${L}require'telescope.builtin'.buffers{}`), // buffer
  PWUFRS: cmd(`${L}require'telescope.builtin'.buffers{}`), // buffers
  PW: cmd(`${L}require'telescope.builtin'.buffers{}`), // b
  TPEUBGS: cmd(`${L}require'telescope.builtin'.quickfix{}`), // fix
  TPEUBGSZ: cmd(`${L}require'telescope.builtin'.quickfixhistory{}`), // fixes
  SKWRUFRP: cmd(`${L}require'telescope.builtin'.jumplist{}`), // jump
  REPBLG: cmd(`${L}require'telescope.builtin'.registers{}`), // rejj
  TPUZ: cmd(`${L}require'telescope.builtin'.current_buffer_fuzzy_find{}`), // fuzz
  TPAOEUPBD: cmd(`${L}require'telescope.builtin'.current_buffer_fuzzy_find{}`), // find
  REF: cmd(`${L}require'telescope.builtin'.lsp_references{}`), // refs
  SPWOL: cmd(`${L}require'telescope.builtin'.lsp_document_symbols{}`), // symbols
  SPWOLZ: cmd(`${L}require'telescope.builtin'.lsp_workspace_symbols{}`), // symbols
  TKPWREP: cmd(`${L}require'telescope.builtin'.live_grep{}`), // grep
  TEBGT: cmd(`${L}require'telescope.builtin'.live_grep{}`), // grep
  HROBG: cmd(`${L}require'telescope.builtin'.loclist{}`), // loc: loclist
  TAOEUP: cmd(`${L}require'telescope.builtin'.filetypes{}`), // types: filetype
  "TP-T": cmd(`${L}require'telescope.builtin'.filetypes{}`), // ft: filetype
  KAUL: cmd(`${L}require'telescope.builtin'.lsp_outgoing_calls{}`), // call
  KAULD: cmd(`${L}require'telescope.builtin'.lsp_incoming_calls{}`), // called
  SEURT: cmd(`${L}require'telescope.builtin'.treesitter{}`), // sitter
  TEL: cmd(`${L}require'telescope.builtin'.builtin{}`), // tel: telescope builtins
  PHRAEPBT: cmd(`${L}require'telescope.builtin'.planets{}`), // planets
  KPHEUT: cmd(`${L}require'telescope.builtin'.git_commits{}`), // commits
  PWRAFRPB: cmd(`${L}require'telescope.builtin'.git_branches{}`), // branches
  EUFRPL: cmd(`${L}require'telescope.builtin'.lsp_implementations{}`), // impl: implementations
  TPHRARB: cmd(`${L}require'flash'.treesitter_search{}`), // flash treesitter
  TKAOEUG: cmd(`${L}require'telescope.builtin'.diagnostics{}`), // diag: diagnostics
  PHARBG: cmd(`${L}require'telescope.builtin'.marks{}`), // marks
  OPGS: cmd(`${L}require'telescope.builtin'.vim_options{}`), // option: vim options
  AUT: cmd(`${L}require'telescope.builtin'.autocommands{}`), // autocommands
  SPEL: cmd(`${L}require'telescope.builtin'.spell_suggest{}`), // spell
  TAG: cmd(`${L}require'telescope.builtin'.tags{}`), // tags
  WEU: cmd(`${L}require'which-key'.show{global=false}`), // which: which-key
  U: cmd(`UndotreeToggle`),
  SPHREUT: cmd(`${L}vs`), // split
  SPHREUTD: cmd(`${L}sp`), // splitted
  TKPWEUT: cmd(`G`), // git: vim-fugitive
  "TKPW*EUT": `${TN}${DLS}:G `, // git: vim-fugitive
  PURB: cmd(`G push`),
  PUL: cmd(`G pull`),
  FEFP: cmd(`G fetch`),
  "STPH-P": `{#Control(w k)}`,
  "STPH-B": `{#Control(w j)}`,
  "STPH-R": `{#Control(w h)}`,
  "STPH-G": `{#Control(w l)}`,
} as const satisfies Record<string, string>;

export async function buildFile() {
  const outputFilename = "vim.json";
  const output: Record<string, string> = {};

  for (const [strokeKey, translation] of Object.entries(map)) {
    let stroke;
    if (strokeKey.startsWith("#")) {
      stroke = `#${starter}${strokeKey.slice(1)}`;
    } else {
      stroke = `${starter}${strokeKey}`;
    }
    const outline = normalizeStenoStrokes([stroke]).join("/");
    output[outline] = translation;
  }

  output[starter] = "{#}";
  for (const [strokesStr, translation] of Object.entries(extras1)) {
    const outline = normalizeStenoStrokes([
      starter,
      ...strokesStr.split("/"),
    ]).join("/");
    output[outline] = translation;
  }

  const outputFile = Bun.file(outputFilename);
  await Bun.write(outputFile, JSON.stringify(output, null, 4));
}

if (import.meta.main) {
  await buildFile();
}
